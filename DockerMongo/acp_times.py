"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

OPEN_SPEEDS = [(1000,26), (600,28), (400,30), (200, 32), (0, 34)]    # creates a table wherein each element is a pair of total brevet distance as well as the max speed between km intervals
CLOSE_SPEEDS = [(1000, 13.333), (600, 11.428), (600, 15), (400, 15), (200, 15), (0, 15)]  # creates a table wherein each element is a pair of total brevet distance as well as the min speed at that distance
MAX_BREV_TIMES = [(200,13.5), (300, 20), (400,27), (600,40), (1000,75)]            # creates a table matching total brevet distances w/ total brevet times

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km < 0:
      return ""

    if control_dist_km > brevet_dist_km:            # if the final control is past the finish line, we use the finish line distance for calculations. source: https://rusa.org/pages/acp-brevet-control-times-calculator
      control_dist_km = brevet_dist_km

    time_taken = 0

    for i in range(len(OPEN_SPEEDS)):
      temp_brev_km = OPEN_SPEEDS[i][0]
      temp_max_speed = OPEN_SPEEDS[i][1]
      while control_dist_km > temp_brev_km:               # want the speed from 1300-1001 to be 26, then from 1000-601 to be 28, so on and so forth
        interval = control_dist_km - temp_brev_km
        time_taken += interval / temp_max_speed    # increment the time, which is dependant on control distance and the max speed at this interval
        control_dist_km -= interval

    sec_shift = (time_taken * 3600) % 60      # will pull out the seconds by converting time to seconds and using modulo
    min_shift = (time_taken * 60) % 60
    hour_shift = int(time_taken)

    brev_open_arrow = arrow.get(brevet_start_time)
    brev_open_arrow = brev_open_arrow.shift(hours = hour_shift, minutes = min_shift, seconds = sec_shift)
    time_open = brev_open_arrow.isoformat()

    return time_open


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km < 0:
      return ""

    time_taken = 0

    if control_dist_km >= brevet_dist_km:            # if the final control is at/past the finish line, we use the end time for calculations. source: https://rusa.org/pages/rulesForRiders
      for i in range(len(MAX_BREV_TIMES)):
        if MAX_BREV_TIMES[i][0] == brevet_dist_km:
          time_taken = MAX_BREV_TIMES[i][1]
    elif control_dist_km <= 60:                     # per piazza post @85, we are to implement the French rules. The French treat controles within the first 60 km differently
      time_taken = 1+ (control_dist_km / 20)
    else:                                              # if the above conditions are not true, then we simply do the same thing we did with open_time(), but with the CLOSE_SPEEDS list
      for i in range(len(CLOSE_SPEEDS)):
        temp_brev_km = CLOSE_SPEEDS[i][0]
        temp_min_speed = CLOSE_SPEEDS[i][1]
        while control_dist_km > temp_brev_km:               # want the speed from 1300-1001 to be 26, then from 1000-601 to be 28, so on and so forth
          interval = control_dist_km - temp_brev_km
          time_taken += (interval) / temp_min_speed    # increment the time, which is dependant on control distance and the max speed at this interval
          control_dist_km -= interval

    sec_shift = (time_taken * 3600) % 60      # will pull out the seconds by converting time to seconds and using modulo
    min_shift = (time_taken * 60) % 60
    hour_shift = int(time_taken)

    brev_close_arrow = arrow.get(brevet_start_time)
    brev_close_arrow = brev_close_arrow.shift(hours = hour_shift, minutes = min_shift, seconds = sec_shift)
    time_close = brev_close_arrow.isoformat()

    return time_close
