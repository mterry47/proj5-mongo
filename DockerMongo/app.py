import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

app = Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brev_start_time = request.args.get("brev_start_time", type=str)
    brev_dist_km = request.args.get("brev_dist_km", type=float)

    message = ""

    if km > (1.2*brev_dist_km):
        message = "The control point ({} km) is over 20 percent longer than the theoretical distance ({} km). Please check units.".format(km, brev_dist_km)
    elif km < 0:
        message = "Invalid control point entered: negative number"

    open_time = acp_times.open_time(km, brev_dist_km, brev_start_time)
    close_time = acp_times.close_time(km, brev_dist_km, brev_start_time)

    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)


@app.route('/display', methods=['POST'])
def _display():                             # provided function, but will come in hand w/ displaying
    _items = db.tododb.find()
    items = [item for item in _items]

    # we want to reset the database after we display so only times will show for this session.
    db.tododb.delete_many({})

    if(items != []):                     # if there are items, we want to display them
        return render_template('display.html', items=items)
    else:
        return render_template('incomplete.html')

@app.route('/incomplete')
def _incomplete():
    app.logger.debug("No entries were supplied")
    return render_template('incomplete.html')

@app.route('/new_entries', methods=['POST'])
def new_entries():
    # we want to reset the database so only times will show for this session
    db.tododb.delete_many({})

    controle_points_km = request.form.getlist("controle_points_km[]")
    controle_points_miles = request.form.getlist("controle_points_miles[]")
    open_times = request.form.getlist("open_times[]")
    close_times = request.form.getlist("close_times[]")
    locations = request.form.getlist("locations[]")

    for i in range(len(controle_points_km)):
        table_entry = {
            'controle_num':str(i+1),                                # we do not want a "controle 0". Start at 1
            'controle_km': str(controle_points_km[i]),
            'controle_mile':  str(controle_points_miles[i]),
            'open_time': str(open_times[i]),
            'close_time': str(close_times[i]),
            'location' : str(locations[i])
        }

        db.tododb.insert_one(table_entry)

    return redirect(url_for('index'))


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

