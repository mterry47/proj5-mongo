# Project 5: Brevet Time Calculator with AJAX and Mongo

## Author: Maxwell H Terry			Email: mterry7@uoregon.edu

Credits to Michal Young and Professor Ram Durairajin for the initial version of this code.

## Brevet Descriptions

A brevet is a timed, long-distance cycling event. Brevets can be (in kilometers): 200, 300, 400, 600, 1000 long. Brevets are denoted by "controle" points wherein cyclers get signed off at specific locations along the event. These controle points are only open during limited intervals. The algorithm is described in the next subsection.

All brevets have maximum time limits, regardless of how many controles there are/where they are. The time limits are as follows (HH:mm): 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM.

## ACP controle times and use

Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   
ACP controle times are dependant on any given controle's distance, the total distance of the brevet, and the start time of the brevet. The opening times are found by dividing the controle's location by the maximum speed over the intervals leading up to that location. The closing times are found by dividing the controle's location by the minimum speed over the intervals leading up to that location..

Intervals:

|Control location (km)	|Minimum Speed (km/hr)  |Maximum Speed (km/hr)|
|:---------------------:|:---------------------:|:-------------------:|
|0 - 200				|	15					|34					  |
|200 - 400				|15						|32					  |
|400 - 600				|15						|30					  |
|600 - 1000				|11.428					|28					  |
|1000 - 1300			|	13.333				|26					  |

In calculating the open/close times, we must change the min/max speed depending on what interval we are in. For example, say we have a 300 km brevet with controles every 50 km. To calculate the controle opening time at 250 km: (50/32) + (200/34) = 7H27. The closing time uses the minimum speeds, so for the controle at 250 km: (250/15) = 16H40.

## Controle Time Oddities

My implementation of this algorithm uses the French variation. This variation states that the maximum time limit for a control within the first 60km is based on a maximum speed of 20 km/hr, plus 1 hour. The motivation behind this variation is to prevent cyclers who start later from being disqualified early. Thus, the controle at the start (0 km) has some opening time, and the close time will be exactly 1 hour after it: 0/20 + 1 = 1 hour

Additionally, if a controle location is past the total distance of the brevet, we just use the maximum distance of the brevet for setting open/close times. If there is a controle at 205 km for a 200 km brevet, then we use 200 km as the control location.

Finally, no controle can be greater than 1.2 * total distance of the brevet away. If you have a brevet of 1000 km, the furthest controle you could possibly have is at (1000 * 1.2) = 1,200 km.

## Credentials.ini

A credentials.ini file will be necessary to run this application. It should go inside the "brevets" directory.

Format of credentials.ini where right-hand of assignment operator is substituted with your data:

author = [name]

repo = [repo_url]

PORT = [port_num]

SECRET_KEY = [secret_key]

## Testing

### Submit Button Tests

#### No Entry Case

1) Once located within localhost:5000/index, press the "Submit" button without any entries in the miles/km fields.

2) User will be redirected to /incomplete, where they will be prompted to return to the table to input values

#### Invalid Entry Case

1) Enter "word", or other non-number, into miles/km field

2) Press enter on keyboard 

3) All fields in row get cleared.

#### Incomplete Entry Case

1) On a blank table, enter "Portland" or other word in the locations box

2) Press "Submit" button

3) User will get redirected to /incomplete, where they will be prompted to return to the table to input values

NOTE: since locations are optional, they can have a value before a user enters a distance

#### Default Form Submission Case

1) On a blank table, enter the number 10 in the km field

2) Press enter

3) Press "Display"

4) User will get redirected to /incomplete

NOTE: by default, due to my implementation, pressing "enter" was triggering a submit. However, we only want a "submit" to occur when the user physically presses the button.

### Display Button Tests

### No Entry Case

1) Once located within localhost:5000/index, press the "Display" button without any entries in the miles/km fields.

2) User will be redirected to /incomplete, where they will be prompted to return to the table to input values

NOTE: the reason for this is I decided to reset the entries in the database on every submit/display, such that only the current session will be reflected in the display

#### Incomplete Entry Case

1) On a blank table, enter "Portland" or other word in the locations box

2) Press "Display" button

3) User will get redirected to /incomplete, where they will be prompted to return to the table to input values

NOTE: since locations are optional, they can have a value before a user enters a distance

## Sources:

- https://rusa.org/pages/acp-brevet-control-times-calculator

- https://rusa.org/pages/rulesForRiders

- https://rusa.org/octime_acp.html